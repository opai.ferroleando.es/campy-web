
<div class="{{ $flex ? 'flex-1' : '' }} {{ $width ?? 'w-full md:w-1/2 lg:w-1/4 xl:w-1/5' }} p-1">
    <a class=" text-gray-900 w-full h-full" href="{{ $event->getPath() }}">
        <div class="bg-gray-200 hover:shadow-md h-full w-full rounded flex flex-col flex-1 p-4">
                <p class="mb-2 text-xl font-semibold">{{ $event->city }} <span class="ml-1 text-sm uppercase font-normal">{{ $event->type }}</span></p>
                <p>
                    @if ($event->date_start == $event->date_end)
                        am {{ date('d.m.y', $event->date_end) }}
                    @else
                        {{ date('d.m.', $event->date_start) }} bis {{ date('d.m.y', $event->date_end) }}
                    @endif
                    <br/>
                    {{ $event->holidays }}
                </p>
                @if ($event->cancelled === 'yes')
                    <p>Leider abgesagt...</p>
                @endif
        </div>
    </a>    
</div>
<div class="mt-6">
    <h2 class="ml-4">{{ $title ?? 'Events' }}</h2>
    <p class="ml-4">{{ $text ?? '' }}</p>
    <div class="flex flex-wrap mt-3">
        @foreach ($events as $event)
            @component('_components.events.card')
                @slot('event', $event)
                @slot('flex', $flex ?? false)
            @endcomponent
        @endforeach
    </div>
</div>
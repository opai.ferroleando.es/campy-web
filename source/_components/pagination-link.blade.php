<a class="block  {{ isset($url) ? 'hover:text-white hover:bg-primary-dark' : 'text-gray-dark bg-gray-200 cursor-not-allowed' }} {{ (isset($active) && $active) ? 'bg-primary-600 text-white' : 'bg-white text-black '}} {{ $border ?? 'border-r' }} px-3 py-2"
    href="{{ $url ?? '#' }}">
    {!! $text ?? '' !!}
</a>

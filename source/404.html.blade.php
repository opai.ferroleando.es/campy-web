@extends('_layouts.master')

@section('body')
        <p class="text-2xl">Das hat leider nicht funktioniert.</p>
        <p>Infos zu den Events gibt es <a href="/events">hier</a>, bist du dir sicher, dass du einen kaputten Link entdeckt hast, freuen wir uns über deine <a href="/kontakt">Nachricht</a>.</p>
@endsection

@section('title')
404 - Fehler
@endsection

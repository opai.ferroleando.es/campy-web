@extends('_layouts.master')

@section('title', '')

@section('body')
        @include('_partials.events.images')

        <div class="mt-8">
            @include('_partials.about')
        </div>

        <div class="mt-8">
            @include('_partials.events.upcoming')
        </div>

        <div class="mt-8">
            @include('_partials.events.testimonials')
        </div>

        <div class="mt-8">
            @include('_partials.video')
        </div>

        <div class="mt-8 mb-4">
            @include('_partials.events.list-done')
        </div>
@endsection

@section('scripts')
    <script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>
        plyr.setup("#plyr-youtube");
    </script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')

@endsection

@extends('_layouts.master')

@section('body')

            <div class="text-xl">
                Wir sind ein gemeinnütziger Verein und können unsere Zwecke und Ziele nur mit eurer Hilfe umsetzen:
                <br>
                Jugendliche, vor allem Mädchen und Jugendliche aus schwierigen sozialen Herkünften, für digitale Berufe zu begeistern.
            </div>

            <div class="mt-8 text-xl">
                Es gibt 3 Möglichkeiten, uns zu unterstützen:
                <ul class="ml-5 list-disc text-xl">
                    <li>Spenden</li>
                    <li>Sponsoring</li>
                    <li>Ehrenamt</li>
                </ul>
            </div>

            <h2 class="mt-8">Spenden</h2>
            <p class="text-xl">Wir sind ein gemeinnütziger Verein, Spenden an uns können steuerlich abgesetzt werden.</p>

            <ul class="ml-5 list-disc">
                <li class="text-xl font-mono">IBAN: DE24100500000190607629</li>
                <li class="text-xl font-mono">BIC: BELADEBEXXX</li>
                <li class="text-xl font-mono">Spende Jugendliche fuer Digitales begeistern</li>
            </ul>

            <h2 class="mt-8">Sponsoring</h2>
            <p class="text-xl">Wir bieten attraktive Pakete im Sponsoring-Bereich "Talent Acquisition/Recruiting" und "Branding/Digital Transformation" an.</p>

            <div class="mt-3 flex items-center bg-primary-100 text-primary-500 text-sm font-bold px-4 py-3" role="alert">
                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" /></svg>
                <p>Für die vier Camps 2020 in Berlin haben wir noch Plätze für Sponsoren.</p>
            </div>

            <h2 class="mt-8">Kontakt</h2>
            <div class="md:flex">
                <div><img src="/img/team/julia.jpg" class="w-32 h-32 rounded-full border-1 border-primary-500"></div>
                <div class="ml-2">
                    <ul>
                        <li class="font-bold text-xl">Julia Hamblin-Trué</li>
                        <li class="text-xl">Head of Partnerships</li>
                        <li class="text-xl"><a href="tel:+49 176 36394041">+49 176 36394041</a></li>
                        <li class="text-xl"><a class="underline" href="mailto:julia@code.design">julia@code.design</a></li>
                    </ul>
                </div>
            </div>
@endsection

@section('title')
Unterstützen
@endsection

@section('style')
<link rel="stylesheet" href="https://app.code.design/css/app.css">
@endsection

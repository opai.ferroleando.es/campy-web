---
active: no
type: Camp
city: Köln
color: hsl(23, 100%, 50%)
color_secondary: gray
date_start: 2019-06-08
date_end: 2019-06-10
days: 3
deadline: 2019-06-01
extends: _layouts.events
section: content
# headerimage: header01.jpg
holidays: Pfingstferien
id: cgn1906
event_id: 15
location: STARTPLATZ Köln, Im Mediapark 5, 50670 Köln
locationlink: https://www.startplatz.de/standorte/
slug: koeln
time_start: '9:00'
time_end: '21:00'
youtube: xGk1PpIbisU
meals: Frühstück, Mittag- und Abendessen
cost: 75€ (0€ wenn Familie es sich nicht leisten kann)
costlaptop: 50€ (0€ wenn Familie es sich nicht leisten kann)
laptopfree: no
teaser: no
partners: yes
supporters: yes
---
<p class="text-lg hyphens mb-6 leading-normal">
Vom 08. bis zum 10 Juni (Pfingstferien) findet in Köln das Code+Design Camp statt. Auf diesem Camp haben Jugendliche im Alter von 14 bis 22 Jahren die Chance an spannenden Hard- und Software Projekten zu arbeiten. Unter der Anleitung von erfahrenen und professionellen Coaches können sie neue Technologien kennen lernen und ihre Fähigkeiten weiter entwickeln. Für die Teilnahme sind keine Vorkenntnisse im Bereich Programmieren und Design notwendig.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Das Camp findet im sog. Hackathon-Format statt. Die Teilnehmer stellen am ersten Tag ihre Projektideen vor, weitere interessierte Teilnehmer schließen sich einem Projekt ihrer Wahl an. Bis zur Abschlusspräsentation am letzten Tag werden in den einzelnen Gruppen Konzepte, Prototypen und teilweise auch fertige Produkte entwickelt. </p>

<p class="text-lg hyphens mb-6 leading-normal">
Während des Camps veranstalten wir auch einen Workshop DEVELOP Your Future, der den Jugendlichen helfen soll, sich aus erster Hand über Berufsbilder und Karriereswege in der Digitalwirtschaft zu informieren.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Während den 3 Tagen werden die Teilnehmer voll verpflegt (Frühstück, Mittagessen und Abendbrot) und nötige Hardware zur Verfügung gestellt. Dies ist möglich durch unsere Sponsoren.
</p>

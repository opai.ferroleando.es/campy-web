---
active: no
cancelled: no
type: Camp
city: Köln
color: hsl(23, 100%, 50%)
color_secondary: gray
date_start: 2019-11-08
date_end: 2019-11-10
days: 3
deadline: 2019-11-06
extends: _layouts.events
section: content
# headerimage: header01.jpg
id: 1911-koe
event_id: 25
location: 'Ambient Innovation: GmbH'
locationlink: 'https://ambient-innovation.com/'
slug: koeln
time_start: 'Fr 16 Uhr'
time_end: 'So 18'
youtube: YXvUe6Nk4D8
meals: Vollverpflegung
cost: 50€ (0€ wenn Familie es sich nicht leisten kann)
costlaptop: 25€ (0€ wenn Familie es sich nicht leisten kann)
laptopfree: no
teaser: no
partners: yes
supporters: yes
---
<p class="text-lg hyphens mb-6 leading-normal">
Vom 08. bis zum 10. November findet in Köln das Code+Design Camp statt. Auf diesem Camp haben Jugendliche im Alter von 14 bis 22 Jahren die Chance an spannenden Hard- und Software Projekten zu arbeiten. Unter der Anleitung von erfahrenen und professionellen Coaches können sie neue Technologien kennen lernen und ihre Fähigkeiten weiter entwickeln. Für die Teilnahme sind keine Vorkenntnisse im Bereich Programmieren und Design notwendig.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Das Camp findet im sog. Hackathon-Format statt. Die Teilnehmer stellen am ersten Tag ihre Projektideen vor, weitere interessierte Teilnehmer schließen sich einem Projekt ihrer Wahl an. Bis zur Abschlusspräsentation am letzten Tag werden in den einzelnen Gruppen Konzepte, Prototypen und teilweise auch fertige Produkte entwickelt. </p>

<p class="text-lg hyphens mb-6 leading-normal">
Während des Camps veranstalten wir auch einen Workshop DEVELOP Your Future, der den Jugendlichen helfen soll, sich aus erster Hand über Berufsbilder und Karriereswege in der Digitalwirtschaft zu informieren.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Während den 3 Tagen werden die Teilnehmer voll verpflegt (Frühstück, Mittagessen und Abendbrot) und nötige Hardware zur Verfügung gestellt. Dies ist möglich durch unsere Sponsoren.
</p>

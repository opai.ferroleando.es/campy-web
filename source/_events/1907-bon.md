---
active: no
type: Camp
city: Bonn
color: hsl(23, 100%, 50%)
color_secondary: gray
date_start: 2019-07-16
date_end: 2019-07-18
days: 3
deadline: 2019-07-16
extends: _layouts.events
section: content
# headerimage: header01.jpg
holidays:
id: 1907-bon
event_id: 19
location: Basketsring 1, 53123 Bonn (Telekom Dome)
locationlink: https://www.telekom-baskets-bonn.de/presse/anfahrt.html
slug: bonn
time_start: '9:00'
time_end: '18:30'
youtube: xGk1PpIbisU
meals: Frühstück, Mittag- und Abendessen
cost: 25€
costlaptop: Bei Bedarf kann ein kostenfreier Laptop ausgeliehen werden
laptopfree: no
teaser: no
partners: yes
supporters: no
register_url:  https://challenge.telekom.com/en/challenges/code-design-telekom-bonn?lang=de
register_text: Registriere dich direkt über unseren Partner.
---
<p class="text-lg hyphens mb-6 leading-normal">
Wir veranstalten mit unserem Partner auch in diesem Jahr wieder ein Camp in Bonn.
Anmelden könnt ihr euch direkt über unseren Partner.
</p>

<div class="text-lg"><p class="mt-2 mb-4">Vom 16.07 bis 18.07.19 findet das Code+Design Camp bei der Deutschen Telekom im T-Dome in Bonn statt. Als Teil der digitalen Transformationsinitiative „IT@Motion“ der Deutschen Telekom IT sollen SchülerInnen und StudentInnen die Möglichkeit erhalten, vier Tage lang an spannenden Hard- und Software-Projekten zu arbeiten. Neben der Weiterentwicklung und Umsetzung eigener Ideen und Konzepte stehen hier insbesondere Teamwork und Kreativität im Vordergrund. Alle interessierten Jugendlichen, Schülerinnen und Schüler im Alter zwischen 14 und 22 Jahren sind willkommen. Für die Teilnahme ist keine Programmiererfahrung notwendig.</p>

<p class="mb-4">Das Camp findet in einem Hackathon-ähnlichen Format statt und soll auch den Mitarbeitern der Deutschen Telekom eine neue, generationsübergreifende Lernerfahrung ermöglichen.
Die Teilnehmer stellen am ersten Tag ihre Projektideen vor, weitere interessierte Teilnehmer schließen sich einem Projekt ihrer Wahl an. Bis zur Abschlusspräsentation am letzten Tag werden in den einzelnen Gruppen Konzepte, Prototypen und teilweise auch fertige Produkte entwickelt. Dabei stehen erfahrene Coaches (Experten der Deutsche Telekom und auch aus der regionalen Wirtschaft) immer dort mit Rat und Tat zur Seite, wo Hilfe und Erfahrung benötigt werden.</p>



<p class="mb-4">Während des Camps werdet Ihr die Möglichkeit haben, die Deutsche Telekom besser kennenzulernen und Euch durch interaktive Workshops über die vielfältigen Jobmöglichkeiten bei uns zu informieren. Das Event findet in der einzigartigen Atmosphäre des Bonner Telekom Domes statt, Heimat der Telekom Baskets. Es erwartet Euch ein tolles Programm mit vielen Überraschungen. Abwechslung und Spaß sind garantiert.
</p>

<p class="mb-4">Die Teilnahme am Camp ist auf maximal 100-120 Plätze begrenzt. Um das Camp aber für alle SchülerInnen, StudentInnen und Coaches erfolgreich durchführen zu können, benötigen wir eine verbindliche Zusage von Euch. Daher erheben wir bei Anmeldung eine Registrierungsgebühr in Höhe von EUR 25.
</p>

<p class="mb-4">Ach ja, für ausreichend Essen, Getränke und sonstige „Knabbereien“ ist natürlich an allen vier Tagen gesorgt. Am besten bringt ihr eigene Laptops, Working Stations oder sonstige Hardware einfach mit. Solltet ihr keinen eigenen Laptops besitzen, sagt uns einfach Bescheid und werden wir Euch die notwendige Hardware gerne bereitstellen.
</p>

<p class="mb-4">Veranstaltet wird das Camp von der gemeinnützigen Code+Design Initiative e.V., die es sich zusammen mit der Deutschen Telekom zum Ziel gesetzt hat, SchülerInnen und StudentInnen an innovative  Technologien heranzuführen und diese über das CODING auch für die neue, digitale Berufswelt zu begeistern. Das Code+Design Camp steht für ein neues, generationsübergreifendes und attraktives Lernformat, welches insbesondere das Interesse von Mädchen und jungen Frauen für diese Bereiche weiter ausbauen möchte.
</p></div>

---
active: no
type: Camp
city: Stuttgart
color: hsl(23, 100%, 50%)
color_secondary: gray
date_start: 2019-08-19
date_end: 2019-08-23
days: 5
deadline: 2019-08-12
extends: _layouts.events
section: content
# headerimage: header01.jpg
holidays: Sommerferien
id: str1908
event_id: 16
location: Merz Akademie (Teckstraße 58, 70190 Stuttgart)
locationlink: https://www.merz-akademie.de/
slug: stuttgart
time_start: '9:00'
time_end: '18:30'
youtube: xGk1PpIbisU
meals: Frühstück, Mittag- und Abendessen
cost: 100€ (0€ wenn Familie es sich nicht leisten kann)
costlaptop: 50€ (0€ wenn Familie es sich nicht leisten kann)
laptopfree: no
teaser: no
partners: yes
supporters: yes
---

<p class="bg-primary-100 p-2 text-xl">
    <b>Achtung! Anmeldung geschlossen, noch per Mail möglich</b><br>Bitte für Last-Minute Anmeldungen kurze Nachricht an <a href="mailto:hello@code.design?subject=Camp%20Stuttgart%3A%20Ich%20will%20noch%20mitmachen&body=Hi%2C%20%0Aich%20will%20noch%20beim%20Camp%20in%20Stuttgart%20mitmachen.%0A%0AHier%20meine%20Daten%3A%0A%0AVor-%20und%20Nachname%0AGeburtsdatum%0AKannst%20du%20dir%20Teilnahmegeb%C3%BChr%20leisten%3A%20ja%2Fnein%0ABrauchst%20du%20Laptop%3A%20ja%2Fnein%0AFalls%20ja%3A%20Kannst%20du%20dir%20Laptopleihgeb%C3%BChr%20leisten%0AErn%C3%A4hrung%3A%20normal%2Fvegetarisch%2Fvegan" class="underline">hello@code.design</a> schicken, dann kümmern wir uns persönlich um dich.
</p>


<p class="text-lg hyphens mb-6 leading-normal">
Vom 19. bis 23. August (Sommerferien) findet in Stuttgart das Code+Design Camp statt. Auf diesem Camp haben Jugendliche im Alter von 14 bis 22 Jahren die Chance an spannenden Hard- und Software Projekten zu arbeiten. Unter der Anleitung von erfahrenen und professionellen Coaches können sie neue Technologien kennen lernen und ihre Fähigkeiten weiter entwickeln. Für die Teilnahme sind keine Vorkenntnisse im Bereich Programmieren und Design notwendig.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Während des Camps veranstalten wir auch einen Workshop DEVELOP Your Future, der den Jugendlichen helfen soll, sich aus erster Hand über Berufsbilder und Karriereswege in der Digitalwirtschaft zu informieren.
</p>

<p class="text-lg hyphens mb-6 leading-normal">
Während den 5 Tagen werden die Teilnehmer voll verpflegt (Frühstück, Mittagessen und Abendbrot) und nötige Hardware zur Verfügung gestellt. Dies ist möglich durch unsere Sponsoren.
</p>

---
pagination:
  collection: magazines
  perPage: 3
---

@extends('_layouts.master')

@section('body')

        <div class="inline-block text-sm">
            @component('_components.pagination', ['pagination' => $pagination])
            @endcomponent
        </div>


        <!-- Start: Articles -->
        <div class="mt-4 flex flex-wrap">
            @foreach($pagination->items as $blog)
                @component('_components.blog.card')
                    @slot('blog', $blog)
                @endcomponent
            @endforeach
        </div>
        <!-- End: Articles -->
@endsection

@section('scripts')
<script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>
        plyr.setup("#plyr-youtube");
    </script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')
Magazin
@endsection

---
# extends: _layouts.master
extends: _layouts.en_coach
section: content

title: Join as a Coach
---

[🇩🇪German](/coach)
[🇬🇧English](/en/coach)


### What's this all about?
- At the Code+Design Camps, 50-75 young people work on project ideas that they bring along themselves.
- The aim of the camps is to get young people with and without previous experience enthusiastic about IT professions.

### What does a coach do?

Coaches on the Code+Design Camps...
- accompany young people in their web, app, gaming, hardware and/or design projects
- help with team building and project management
- inspire as practitioners from the tech industry - whether as a professional or student, employee or self-employed and are (optionally) available for career orientation as a discussion partner
- organize (optional) practical workshops for beginners and advanced students through

### How can I support as a coach?
- You can coach in 2 areas: content and/or team-related.
- Content: You help with technical questions and may also offer a workshop.
- Team: You are the contact person for 1-2 teams and help them to achieve their goals.
- As a coach you should be there at least 1 day

### What happens (from a coach's point of view) at the camps?
- Day 1: Inspiring and advising: You get young people excited about tech by showing them what you like to do (open source, company projects...). As a coach, you support teams in choosing the right technology for them and in finding learning material.
- Subsequent days: The coaches offer 1.5 hours of consultation per camp day for problems in the projects (technology, learning, team). They also organise interactive 1.5h workshops on topics related to the web, mobile and games.


<div class="flex mt-10">
    <a
        href="/en/coach/faq"
        class="inline-block  mt-4 mb-4 mr-8 border border-black bg-white p-4 text-black hover:bg-primary hover:text-white hover:border-primary-500">
        View Questions and Answers
    </a>

    <a
        href="/kontakt"
        class="inline-block  mt-4 mb-4 border border-primary bg-primary-500 p-4 text-white hover:bg-primary-600"
        >I want to be a coach</a>
</div>

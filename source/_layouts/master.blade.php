<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <title>Code+Design | @yield('title')</title>

        <!-- Start: Favicons -->
        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#A000BA">
        <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#A000BA">
        <!-- End: Favicons -->

        <!-- Start: Open Graph -->
        <meta property="og:title" content="@yield('og:title')" /> <!-- Same title as in line 5 ? -->
        <meta property="og:description" content="@yield('og:description')" /> <!-- Same description as in line 14 ? -->
        <meta property="og:url" content="@yield('og:url')" />
        <meta property="og:image" content="@yield('og:image')" />
        <!-- End: Open Graph -->

        <!-- Start: Twitter Card -->
        <meta name="twitter:card" content="@yield('twitter:card')">
        <meta name="twitter:site" content="@yield('twitter:site')">
        <meta name="twitter:creator" content="@yield('twitter:creator')">
        <meta name="twitter:title" content="@yield('twitter:title')"> <!-- Same title as in line 5 ? -->
        <meta name="twitter:description" content="@yield('twitter:description')"> <!-- Same description as in line 14 ? -->
        <meta name="twitter:image" content="@yield('twitter:image')">
        <!-- End: Twitter Card -->

        <!-- Start: Other meta -->
        <meta name="description" content="@yield('description')">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <!-- End: Other meta -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
        <link rel="stylesheet" href="https://app.code.design/font/lemon_milk/stylesheet.css">

        <!-- Start: Styles -->
        <style>
            @yield('styles');
        </style>
        <!-- End: Styles -->
    </head>
    <body class="font-sans antialiased bg-pattern sm:px-2 lg:px-6 bg-primary-100">

        <div class="container mx-auto">
            @include('_partials.navbar')
    
            <main class="sm:my-2 lg:my-6 px-3 sm:p-4 lg:p-8 sm:rounded bg-white">
                @if($__env->yieldContent('title') != '')
                <h1 class="text-primary-900">@yield('title')</h1>
                @endif
                @yield('body')
            </main>
    
            @include('_partials.footer')
        </div>

        <!-- Start: Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/lazysizes@4.0.1/lazysizes.min.js"></script>
        @yield('scripts')
        <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
        <!-- End: Scripts -->

        @if($page->production)
        <!-- Fathom - simple website analytics - https://github.com/usefathom/fathom -->
        <script>
        (function(f, a, t, h, o, m){
            a[h]=a[h]||function(){
                (a[h].q=a[h].q||[]).push(arguments)
            };
            o=f.createElement('script'),
            m=f.getElementsByTagName('script')[0];
            o.async=1; o.src=t; o.id='fathom-script';
            m.parentNode.insertBefore(o,m)
        })(document, window, '//analytics.code.design/tracker.js', 'fathom');
        fathom('set', 'siteId', 'CMHOV');
        fathom('trackPageview');
        </script>
        <!-- / Fathom -->
        @endif

    </body>
</html>

@extends('_layouts.master')

@section('body')
@if ($page->active === 'no')
<div class="bg-primary-100 text-primary-900 w-full p-2">
    @if ($page->cancelled === 'yes')
    Dieses {{ $page->type }} wurde abgesagt.
    @else
    Dieses {{ $page->type }} ist leider schon vorbei…
    @endif
</div>
@endif

<header class="lg:flex">
    <div class="w-full lg:w-1/2">
        @if ($page->youtube)
        <div id='plyr-youtube' data-type="youtube" data-video-id="{{ $page->youtube }}" class="rounded"></div>
        @else
        <div><img src="/img/{{ $page->headerimage }}" alt=""></div>
        @endif
    </div>
    <div class="w-full lg:w-1/2 ml-0 lg:ml-4 lg:border lg:border-gray-300 lg:p-3 rounded-sm">
        @include('_partials.events.info')
    </div>

</header>

<div class="md:flex mt-4">
    <div class="w-full md:w-1/2">
    <h2>Das Code+Design Camp</h2>
        @if ($page->teaser === 'yes')
        @component('_components.events.teaser')
        @slot('location') {{ $page->location }} @endslot
        @slot('from') {{ date('d.m.y', $page->date_start) }} @endslot
        @slot('to') {{ date('d.m.y', $page->date_end) }} @endslot
        @slot('city') {{ $page->city }} @endslot
        @slot('holidays') {{ $page->holidays }} @endslot
        @slot('days') {{ $page->days }} @endslot
        @endcomponent
        @endif
        {!! $page->getContent() !!}
    </div>
    <div class="w-full md:w-1/2 md:ml-8">
        <div>
            @include('_partials.faq', ['cost' => $page->cost, 'costlaptop' => $page->costlaptop, 'event_id' => $page->id])
        </div>
    </div>
</div>



<section id="partner">

    @if($page->supporters == 'yes')
    <div class="text-5xl mt-8 text-primary-900 font-bold uppercase font-sans">Förderer</div>
    <div class="border border-1 border-gray-300 rounded p-8">
        <div class="flex flex-wrap ">
            @foreach ($partners->filter->hasEvent($page->id) as $partner)
            @if($partner->tier == 'silver' or $partner->tier == 'gold' or $partner->tier == 'bronze')
            <div class="p-4">
                <div class="flex flex-col items-center">
                    <div class="flex flex-col justify-center">
                        <a href="{{ $partner->website}}">
                            @component('_components.img')
                            @slot('src') /img/partner/{{ $partner->logo }} @endslot
                            @slot('alt') {{ $partner->name }} @endslot
                            @slot('width') {{ $partner->width }} @endslot
                            @slot('height') {{ $partner->height ?? 'h-16'}} @endslot
                            @endcomponent
                        </a>
                    </div>
                    @if($page->long)
                    {{ $partner->long }}
                    @endif
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
    @endif


    @if($page->partners == 'yes')
    <div class="text-5xl mt-8 text-primary-900 font-bold uppercase font-sans">Partner</div>
    <div class="border border-1 border-gray-300 rounded p-8">
        <div class="flex flex-wrap ">
            @foreach ($partners->filter->hasEvent($page->id) as $partner)
            @if ($partner->tier == 'patron' or $partner->tier == 'partner' )
            <div class="p-4">
                <div class="flex flex-col items-center">
                    <div class="flex flex-col justify-center">
                        <a href="{{ $partner->website}}">
                            @component('_components.img')
                            @slot('src') /img/partner/{{ $partner->logo }} @endslot
                            @slot('alt') {{ $partner->name }} @endslot
                            @slot('width') {{ $partner->width }} @endslot
                            @slot('height') {{ $partner->height ?? 'h-16'}} @endslot
                            @endcomponent
                        </a>
                    </div>
                    @if($page->long)
                    {{ $partner->long }}
                    @endif

                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
    @endif
</section>



@endsection

@section('meta')

@if ($page->youtube)
<link rel="stylesheet" href="/css/plyr.css">
@endif

@endsection


@section('scripts')

<script type='application/ld+json'>
    {
        "@context": "https://www.schema.org",
        "@type": "Event",
        "name": "Code+Design {{ $page->type }} {{ $page->city }}",
        "url": "{{ $page->getUrl() }}",
        "typicalAgeRange": "14-22",
        "description": "Das Code+Design Camp für Jugendliche in {{ $page->city }}. Kreativer Hackathon – von der Idee zu eigener Webseite, App oder eigenem Spiel in kürzester Zeit.",
        "startDate": "{{ date('Y-m-d', $page->date_start) }}",
        "endDate": "{{ date('Y-m-d', $page->date_end) }}",
        "location": {
            "@type": "Place",
            "name": "{{ $page->location }}",
            "sameAs": "{{ $page->locationlink }}",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "{{ $page->location }}",
                "addressLocality": "{{ $page->city }}",
                "addressCountry": "DE"
            }
        }
    }
</script>

@if ($page->youtube)
<script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
<script>
    plyr.setup("#plyr-youtube");
</script>
@endif

@endsection


@section('title')
{{ $page->type }} | {{ $page->city }} @if ($page->days > 1)({{ date('d.m.', $page->date_start) }} - {{ date('d.m.y', $page->date_end) }} @else {{ date('d.m.', $page->date_start) }} @endif)
@endsection

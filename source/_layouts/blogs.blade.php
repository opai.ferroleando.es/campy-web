@extends('_layouts.master')

@section('body')

            <div class="container mx-auto clearfix">
            <p class="italic text-gray-800">Veröffentlicht am: {{ date('d.m.Y', $page->published_at) }}, Autor: {{ $page->author }} @if ($page->image_source), Bildquelle: {{ $page->image_source }} @endif</p>
                <div class="rich-text mt-4 text-lg leading-normal">
                    <div class="md:ml-5 mb-1 mx-auto md:float-right w-full md:max-w-sm">
                            @if($page->youtube <> '')
                            <div id="plyr-youtube" data-type="youtube" data-video-id="{{$page->youtube}}"></div>
                        @elseif($page->image <> '')
                            @component('_components.img')
                                @slot('src') {{$page->image}} @endslot
                            @endcomponent
                        @endif
                    </div>
                {!! $page->getContent() !!}
                </div>
                <div class="flex flex-col sm:flex-row mt-8">
                        <div>
                            @if ($page->getPrevious())
                                @component('_components.button')
                                    @slot('url', $page->getPrevious()->getPath())
                                    @slot('width', 'w-full sm:w-auto')
                                    &larr; Vorheriger Post
                                @endcomponent
                            @endif
                        </div>
                        <div class="mt-6 sm:mt-0 sm:ml-auto">
                            @if ($page->getNext())
                                @component('_components.button')
                                    @slot('url', $page->getNext()->getPath())
                                    @slot('width', 'w-full sm:w-auto')
                                    Nächster Post &rarr;
                                @endcomponent
                            @endif
                        </div>
                    </div>
            </div>
@endsection

@section('scripts')
<script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
<script>
    plyr.setup("#plyr-youtube");
</script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')
Magazin | {{$page->caption}}
@endsection

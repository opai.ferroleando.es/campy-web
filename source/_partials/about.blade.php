<div name="card" class="flex flex-wrap justify-between rounded text-gray-900">
    <div class="md:w-1/2 p-4">
        <div class="flex items-center">
            <img src="/img/about/teamwork.png" class="h-12">
            <h3 class="ml-2">Eigene Projektideen im Team  verwirklichen</h3>
        </div>
        <p class="mt-3 text-lg leading-normal">Egal, ob du eine Homepage für deinen Verein oder Clan kreierst, eine App für die Schule baust, oder in die Welt von Smart-Homes und Internet of Things eintauchst – du kannst an genau den Projekten und Themen arbeiten, die dich interessieren.</p>
    </div>

    <div class="md:w-1/2 p-4">
    <div class="flex items-center">
        <img src="/img/about/coaching.png" class="h-12">
        <h3 class="ml-2">Gecoacht von echten Profis!</h3>
    </div>

      <p class="mt-3 text-lg leading-normal">Es spielt keine Rolle, ob du schon erfahren im Programmieren bist oder ob du auf dem Code+Design Camp deine ersten Schritte in der digitalen Welt gehst: Unsere Coaches haben genau die richtigen Tipps und Workshops für dich parat!</p>
    </div>
    <div class="md:w-1/2 p-4">
    <div class="flex items-center">
        <img src="/img/about/spass.png" class="h-12">
        <h3 class="ml-2">In netter Atmosphäre</h3>
    </div>
    <p class="mt-3 text-lg leading-normal">Zusammen im Team stellt ihr etwas Großartiges auf die Beine. Im Team mit Anfängerinnen und Fortgeschrittenen. Und alle helfen einander. Viele finden dabei neue Freunde.</p>
    </div>
    <div class="md:w-1/2 p-4">
    <div class="flex items-center">
        <img src="/img/about/compass.png" class="h-12">
        <h3 class="ml-2">Berufsorientierung inklusive</h3>
    </div>
      <p class="mt-3 text-lg leading-normal">Auf jedem Camp gibt es einen Berufsorientierungsworkshop, auf dem einige Coaches ihren Beruf und Werdegang vorstellen und alle Fragen der Teilnehmerinnen und Teilnehmer beantworten.</p>
    </div>
  </div>

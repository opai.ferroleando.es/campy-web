
@component('_components.img')
    @slot('src', '/img/header03.jpg')
    @slot('alt', 'Code+Design Event')
    @slot('width', 'w-full')
@endcomponent

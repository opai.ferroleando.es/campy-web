<div class="mb-4 mt-4">
    <div class="text-3xl mb-4 mt-8 ml-4">
        <h2 class="font-xl font-bold text-gray-900"><a href="/initiative/stimmen" >Das Feedback</a></h2>
    </div>
    <div class="flex flex-wrap items-stretch">
      @foreach ($testimonials->take(3) as $testimonial)
            <div class="w-full md:w-1/3 p-1">
                @component('_components.card')
                    @slot('src', $testimonial->src)
                    @slot('url', $testimonial->resource)
                    @slot('medium', $testimonial->medium)
                    @slot('channel', $testimonial->channel)
                    @slot('published_at', date('d.m.y', $testimonial->published_at))
                    {{ $testimonial->content }}
                @endcomponent
            </div>
      @endforeach
    </div>
</div>

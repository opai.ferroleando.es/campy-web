<div class="bg-gray-400 p-4 rounded">
    <h3 class="mb-4">Interesse dabeizusein?</h3>
    <form action="https://app.code.design/api/interest" method="POST">

        @include('_partials.honeypot')
        <input type="hidden" name="return_path" value="https://code.design/anfrage">
        <input type="hidden" name="error_path" value="{{ $page->getPath() }}">

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full sm:w-1/2 lg:w-auto lg:flex-1 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wider text-gray-800 text-xs font-bold mb-2" for="grid-city">
                    Vorname
                </label>
                <input class="appearance-none block w-full bg-gray-lighter text-gray-800 border border-gray-200 rounded py-3 px-4" name="firstname" type="text" placeholder="Simone" required pattern="[a-zA-Z\- ]{2,}">
            </div>
            <div class="w-full sm:w-1/2 lg:w-auto lg:flex-1 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wider text-gray-800 text-xs font-bold mb-2" for="grid-zip">
                    Email
                </label>
                <input class="appearance-none block w-full bg-gray-lighter text-gray-800 border border-gray-200 rounded py-3 px-4" name="email" type="email" placeholder="robot@simonegiertz.com" required>
            </div>
            <div class="w-full sm:w-1/2 lg:w-auto lg:flex-1 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wider text-gray-800 text-xs font-bold mb-2" for="city">
                    Stadt
                </label>
                <div class="relative">
                    <select class="block appearance-none w-full bg-gray-lighter border border-gray-200 text-gray-800 py-3 px-4 pr-8 rounded" name="city" required>
                        <option value="" selected>Bitte auswählen...</option>
                        <option value="ber">Berlin</option>
                        <option value="due">Düsseldorf</option>
                        <option value="koe">Köln</option>
                        <option value="hh">Hamburg</option>
                        <option value="lei">Leipzig</option>
                        <option value="mue">München</option>
                        <option value="stu">Stuttgart</option>
                        <option value="fra">Frankfurt</option>
                        <option value="all">Alle</option>
                        <option value="other">Andere</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-800">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div>
            <div class="self-end px-3">
                <button class="px-3 mb-6 md:mb-0  py-3 text-gray-900 mr-4 rounded bg-gray-500  hover:bg-gray-600">Senden</button>
            </div>
        </div>
    </form>
</div>

<div class="flex flex-wrap -mx-3 sm:mx-0">
        <div class="w-full md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/002.jpg')
                @slot('alt', 'Jugendliche schauen auf Monitor')
                @slot('rounded', 'rounded-0 sm:rounded-l')
            @endcomponent
        </div>
        <div class="hidden md:block md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/004.jpg')
                @slot('alt', 'Jugendliche begutachten Raspberry Pi')
                @slot('rounded', 'rounded-0')
            @endcomponent
        </div>
        <div class="hidden md:block md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/005.jpg')
                @slot('alt', 'Mädchen arbeiten am Computer')
                @slot('rounded', 'rounded-0 rounded-r')
            @endcomponent
        </div>
    </div>

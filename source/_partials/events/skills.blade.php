@component('_components.pill-list')
    @slot('pills', [
        'Programmier- & Designevent',
        'Für Jugendliche zwischen 14 und 22',
        'Deine Ideen',
        'Hard- & Software',
        'Projekte',
        'Für Einsteiger & Profis',
        'Berufsorientierung',
        'Präsentationen',
        'Teamarbeit',
        'Neue Freunde finden',
        'Coaches aus der Praxis'
    ])
@endcomponent

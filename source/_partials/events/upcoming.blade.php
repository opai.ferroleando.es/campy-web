@if($events->where('active', 'yes')->count() > 0)
    @component('_components.events.row')
        @slot('title', 'Nächste Events')
        @slot('text', 'Hier stehen alle Events, die schon fix sind und für die du dich anmelden kannst.')
        @slot('events', $events->where('active', 'yes'))
    @endcomponent
@else
    <div class="border-2 rounded p-4 bg-gray-200">
        <h2 class="font-bold text-xl mb-4 text-gray-900">Nächste Events</h2>
        <p>Wir sind gerade in der Planung für weitere Events, bitte gedulde dich etwas!</p>
    </div>
@endif

<div class="mt-4 mb-4 p-4 bg-primary-100 rounded-lg">
    {{$register_text ?? ""}} <br>
    <a href="{{ $register_url ?? ('https://app.code.design/events/' . $page->event_id . '/register') }}" class="bg-primary-500 text-white  inline-block text-3xl font-bold px-4 py-2 leading-none rounded-lg hover:border-transparent hover:bg-primary-600 mt-4" target="_blank">
        Anmelden
    </a>
    <br>
</div>

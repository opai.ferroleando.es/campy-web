@extends('_layouts.master')

@section('body')
    <div class="bg-white rounded p-8">
        <div class="text-3xl">
            Danke! Du erhältst jetzt unseren Newsletter.
        </div>

@endsection

@section('title')
Newsletter
@endsection

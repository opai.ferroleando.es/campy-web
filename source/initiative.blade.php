@extends('_layouts.master')

@section('body')
    <div>
        <p class="text-lg leading-normal">
            Die gemeinnützige Code+Design Initiative hat sich zum Ziel gesetzt, Jugendliche für digitale Technologien und Berufe zu begeistern
            und insbesondere den Anteil an Frauen in diesen Bereichen zu erhöhen. Dafür führt sie Code+Design Camps in verschiedenen Orten durch
            und gibt ein Magazin mit IT-Themen für Jugendliche heraus.
        </p>
    </div>

    @include('_partials.team.section', ['title' => 'Team', 'type' => 'core'])
    @include('_partials.team.section', ['title' => 'Kuratorium', 'type' => 'kuratorium'])
    @include('_partials.team.section', ['title' => 'Ehrenamtliche UnterstützerInnen', 'type' => 'helper'])

    <div class="mt-4">
        @include('_partials.donations')
    </div>
@endsection

@section('title')
Initiative
@endsection

---
extends: _layouts.partners
name: SWYX
logo: swyx.png
strength: 90
tier: partner
events: 1908-dor
website: https://www.swyx.de/
instagram:
twitter:
facebook:
---

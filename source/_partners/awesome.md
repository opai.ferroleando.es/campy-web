---
extends: _layouts.partners
name: Awesome Software
logo: awesome.svg
strength: 90
tier: partner
events: 1907-monta
website: http://www.awesome-software.de/
instagram:
twitter:
facebook:
---

---
extends: _layouts.partners
name: Ambient Innovation
logo: ambientinnovation.png
strength: 90
tier: partner
events: 1911-koe
website: https://ambient-innovation.com
instagram:
twitter:
facebook:
---

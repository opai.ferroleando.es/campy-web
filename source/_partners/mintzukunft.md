---
extends: _layouts.partners
name: MINT Zukunft e.V.
logo: mint-zukunft.svg
tier: partner
strength: 200
events:
website: https://mintzukunftschaffen.de/
instagram:
twitter:
facebook:
---
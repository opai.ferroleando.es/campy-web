---
extends: _layouts.partners
name: Bechtle
logo: bechtle.png
strength: 90
tier: partner
events: 1908-dor
website: https://www.bechtle.com/
instagram:
twitter:
facebook:
---

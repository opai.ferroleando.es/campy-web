---
extends: _layouts.partners
name: UnityMedia
logo: unitymedia.jpg
website: https://www.unitymedia.de/
tier: gold
strength: 1000
events: koe1706,ffm1708,stu1707,koe1805,ffm1810,cgn1906,str1908,1911-koe,1909-koe
short:
height: h-32
---

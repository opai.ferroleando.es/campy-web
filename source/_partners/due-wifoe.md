---
extends: _layouts.partners
name: Wirtschaftsförderung Düsseldorf
logo: wifo-due.jpg
strength: 91
tier: partner
events: due1803
website: https://www.startupwoche-dus.de
instagram:
twitter:
facebook:
---
---
extends: _layouts.partners
name: Dortmunder Volksbank
logo: dortmunder-volksbank.png
strength: 90
tier: partner
events: 1908-dor
website: https://www.dovoba.de/
instagram:
twitter:
facebook:
---

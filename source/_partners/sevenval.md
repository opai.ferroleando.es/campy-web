---
extends: _layouts.partners
name: Sevenval
logo: sevenval.png
strength: 90
tier: partner
events: ber1710,koe1706
website: https://sevenval.com/de
instagram:
twitter:
facebook:
---
Sevenval unterstützt uns mit supertollen Coaches für den Frontendbereich.
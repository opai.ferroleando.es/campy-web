---
extends: _layouts.partners
name: Stadt Dortmund Wirtschaftsförderung
logo: stadtdortmund-wf.jpg
strength: 90
tier: partner
events: 1908-dor
website: https://www.wirtschaftsfoerderung-dortmund.de/
instagram:
twitter:
facebook:
---

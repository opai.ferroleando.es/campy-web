---
extends: _layouts.partners
name: Dogado Information & Communication
logo: dogado.png
strength: 90
tier: partner
events: 1908-dor
website: https://dogado.de
instagram:
twitter:
facebook:
---

module.exports = {
  important: true,
  theme: {
    fontFamily: {
        'lemonmilk': 'LemonMilk',
        'lemonmilklight': 'LemonMilkLight',
        'lemonmilklightitalic': 'LemonMilkLightItalic',
        'lemonmilkitalic': 'LemonMilkItalic',
        'lemonmilkbold': 'LemonMilkBold',
        'lemonmilkbolditalic': 'LemonMilkBoldItalic',
        sans: ['Source Sans Pro', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI',
            'Roboto', 'Helvetica Neue','sans-serif'],
    },
    extend: {
      colors: {
        primary: {
          100: '#F1E6F8',
          200: '#DDBFEE',
          300: '#C899E3',
          400: '#9F4DCF',
          500: '#A000BA',
          600: '#7C008F',
          700: '#600070',
          800: '#3F003C',
          900: '#160015',
        },
      },
      margin: {
        '96': '24rem',
        '128': '32rem',
      },
    }
  },
  variants: {
    opacity: ['responsive', 'hover']
  }
}
